# this script checks mastodon database for new users and sends them a welcome message through mastodon API...

# library for date and time functions
import datetime
# library for PostgreSQL interaction
import psycopg2
# library for access to Mastodon API
from mastodon import Mastodon
# library for serialization to store variable on disk
import pickle


# creates login credentials (only needs to be run one time)
def create_creds(instance):
    Mastodon.create_app(
        'pytooterapp',
        api_base_url='https://' + instance,
        to_file='pytooter_clientcred.secret'
    )
    mastodon = Mastodon(
        client_id='pytooter_clientcred.secret',
        api_base_url='https://' + instance
    )
    # !!! Here is where you need to fill in the username + password in clear-text !!!
    mastodon.log_in(
        'user-email@example.com',
        'secret-password',
        to_file='pytooter_usercred.secret'
    )


# returns the last query timestamp from a file
def get_last_query(filename):
    # load last query timestamp from local file...
    infile = open(filename, 'rb')
    last_query_timestamp = pickle.load(infile)
    infile.close()
    print('Last timestamp: ' + str(last_query_timestamp))
    return str(last_query_timestamp)


# updates the last query timestamp in a file
def update_last_query(filename, new_query_timestamp):
    # update last query timestamp in the file
    outfile = open(filename, 'wb')
    pickle.dump(new_query_timestamp, outfile)
    outfile.close()


# returns new users in an array
def discover(last_query_timestamp):
    found_users = []
    new_query_timestamp = datetime.datetime.now()
    print('New timestamp: ' + str(new_query_timestamp))

    # query new users from the database
    conn = psycopg2.connect(
        # !!! Enter database connection details below !!!
        host="localhost",
        database="mastodon",
        user="mastodon",
        # !!! Be careful with this clear-text password !!!
        password="secret-password")
    cur = conn.cursor()
    cur.execute("SELECT account_id FROM users WHERE created_at >= '" + last_query_timestamp + "'")
    new_users = cur.fetchall()
    for new_user in new_users:
        # print(new_user[0])
        cur.execute("SELECT username FROM accounts WHERE id='" + str(new_user[0]) + "'")
        new_user_handle = cur.fetchone()
        found_users.append(new_user_handle[0])
    cur.close()
    conn.close()
    # write last query timestamp to a file
    update_last_query('lastQueryTimestamp', new_query_timestamp)

    # return all found users in an array
    return found_users


# Toots the new user with a Welcome message
def welcome(instance, username):
    # connect to instance
    mstdn = Mastodon(
        access_token='pytooter_usercred.secret',
        api_base_url='https://' + instance
    )
    mstdn.toot('Welcome to #IOCX @' + username + '!\n\nCheck out our Getting Started Guide here: '
                                                 'https://ioc.wiki/mastodon \n\n@seb is your admin - Questions? Ask '
                                                 'him! \n\nHave FUN and stay safe!')


# main loop
if __name__ == '__main__':
    # Put in your instance name here:
    mastodon_instance = 'ioc.exchange'
    # create_creds(mastodon_instance)

    # Let's see what new users we can find...
    new_user_handles = discover(get_last_query('lastQueryTimestamp'))
    print(new_user_handles)

    # Send the Welcome Toots
    for user_handle in new_user_handles:
        welcome(mastodon_instance, user_handle)

