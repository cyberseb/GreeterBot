# GreeterBot

A Mastodon Bot that greets newly registered users.

## Dependencies

Use PIP to install the two external libraries:

    pip3 install psycopg2-binary
    pip3 install Mastodon.py
    

## How do I run it..?

I run it on the Mastodon Database Server via crontab of the mastodon user every 5 minutes:

    */5 * * * *     run_greeter.sh


Here is the shell script (run_greeter.sh) I use to run this Python script via crontab:

    cd /home/mastodon/GreeterBot
    python3 greeter.py >> greeter.log


The Log output for no new user found, looks like this:

    $ cat GreeterBot/greeter.log 
    Last timestamp: 2021-07-05 03:15:09.117215
    New timestamp: 2021-07-05 03:20:36.146875
    []
    Last timestamp: 2021-07-05 03:20:36.146875
    New timestamp: 2021-07-05 03:25:02.082494
    []

